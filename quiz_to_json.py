import os
import re
from telethon import TelegramClient, sync
import json
from time import sleep
import random

class telegram_handler:

    def __init__(self,api_id,api_hash,phone,session_name):
        """
        Конструктор класса устанавливает соединение с telegram аккаунтом согласно переданным параметрам
        """
        self.api_id = api_id
        self.api_hash = api_hash
        self.phone = phone
        self.session_name = session_name

        #папка назначения
        self.dir_name = ''

        #данные для запуска квиза
        self.__quiz_part_command = '/view_'
        self.__quiz_bot_username = 'QuizBot'

        #метаданные квиза
        self.__quiz_meta_arr = []
        self.__quiz_name = ''
        self.__quiz_desc = ''
        self.__quiz_count = 0
        self.__quiz_time_per_question = ''

        #данные текущего вопроса из квиза
        self.__question_name = ''
        self.__answers_arr = []
        self.__correct_answer = 0
        self.__solution = ''

        #начало теста
        self.start_id = 0


        #название текущей мультимедии
        self.media_name = []
        self.media = None
        self.media_flag = False

        #словарь и данные для json
        self.__quiz_json = []

        self.client = TelegramClient(self.session_name, self.api_id, self.api_hash)
        self.client.start()

    def quiz_to_json(self, tg_link):
        last_message_id = 0

        begin_of_quiz_index = tg_link.index('=') + 1
        quiz_tg_command = tg_link
        #запускаем тест
        self.__send_msg_to_bot(tg_link)

        messages = self.client.get_messages(self.__quiz_bot_username)
        last_message_id = messages[0].id
        while messages[0].message == quiz_tg_command:
            sleep(1)
            messages = self.client.get_messages(self.__quiz_bot_username)
            last_message_id = messages[0].id

        # нажимаем на 'Пройти тест'
        if messages[0].message.split(' ')[1] == 'Тест':
            messages[0].click(0)
            messages = self.client.get_messages(self.__quiz_bot_username)
            while messages[0].id == last_message_id:
                sleep(1)
                messages = self.client.get_messages(self.__quiz_bot_username)

        self.__meta_quiz_handler(messages[0].message)
        self.start_id = messages[0].id
        self.__meta_to_json()

        #нажимаем на 'Пройти тест' (вторая кнопка)
        messages[0].click(0)
        #дожидаемся пока не будет получен первый опрос
        messages = self.client.get_messages(self.__quiz_bot_username)
        #отбрасываем метаданные
        while messages[0].media == None:
            sleep(1)
            messages = self.client.get_messages(self.__quiz_bot_username)

        #отбрасываем возможную мультимедию
        while str(type(messages[0].media)) != "<class 'telethon.tl.types.MessageMediaPoll'>":
            sleep(2)
            messages = self.client.get_messages(self.__quiz_bot_username)

        # self.__empty_quiz_to_json()

        while messages[0].media != None:
            sleep(1)
            messages = self.client.get_messages(self.__quiz_bot_username, limit=2)
            #скачиваем мультимедию если таковая имеется
            if self.start_id != messages[1].id and str(type(messages[1].media)) != "<class 'telethon.tl.types.MessageMediaPoll'>":
                self.media = messages[1]
                self.media_flag = True
            # голосуем в квизе
            temp_id = messages[0].id
            messages[0].click(0)
            #восстановление на случай провала соединения
            messages = self.client.get_messages(self.__quiz_bot_username,limit=3)
            for m in messages:
                if m.id == temp_id:
                    messages = []
                    messages.append(m)
            if str(type(messages[0].media)) != "<class 'telethon.tl.types.MessageMediaPoll'>":
                sleep(4)
                messages = self.client.get_messages(self.__quiz_bot_username, limit = 5)
                temp = messages[-1]
                messages = []
                messages.append(temp)
            last_message_id = messages[0].id
            #извлечение данных опроса
            #извлекаем имя опроса
            temp_quiz_name = ''
            temp_arr = messages[0].media.poll.question.split(' ')[1:]
            if len(temp_arr) > 1:
                for part_name_quiz in messages[0].media.poll.question.split(' ')[1:]:
                    temp_quiz_name += part_name_quiz
                    temp_quiz_name += ' '
            else:
                temp_quiz_name = messages[0].media.poll.question.split(' ')[1]
            self.__quiz_name = temp_quiz_name
            #извлекаем варианты ответа
            for option in messages[0].media.poll.answers:
                self.__answers_arr.append(option.text)
            #извлекаем правильный ответ
            for answer in messages[0].media.results.results:
                if answer.correct == True:
                    self.__correct_answer = int(str(answer.option)[2:-1])
            #извлекаем подсказку если таковая имеется
            if messages[0].media.results.solution is not None:
                self.__solution = messages[0].media.results.solution
            else:
                self.__solution = ''
            #добавляем в json новый опрос
            self.__pull_to_json(self.__quiz_name, self.__answers_arr, self.__correct_answer)
            #ожидаем следующий опрос
            #отбрасываем старые опросы
            while messages[0].id == last_message_id:
                messages = self.client.get_messages(self.__quiz_bot_username)
                sleep(1)
            #если опросы закончились выходим из цикла
            if messages[0].media == None:
                sleep(2)
                messages = self.client.get_messages(self.__quiz_bot_username)
                if str(type(messages[0].media)) != "<class 'telethon.tl.types.MessageMediaPoll'>":
                    break

            #отрабсываем возможную мультимедию
            while str(type(messages[0].media)) != "<class 'telethon.tl.types.MessageMediaPoll'>":
                messages = self.client.get_messages(self.__quiz_bot_username)
                sleep(2)

        self.__data_to_json(self.__quiz_json)

    def __meta_quiz_handler(self, msg):
        for msg_part in msg.split('\n'):
            if msg_part != '':
                self.__quiz_meta_arr.append(msg_part)

        if len(self.__quiz_meta_arr) > 6:
            #извлекаем имя квиза
            self.__quiz_name = self.__quiz_meta_arr[0][self.__quiz_meta_arr[0].index('«')+1:-1]
            #извлекаем описание квиза
            self.__quiz_desc = self.__quiz_meta_arr[1]
            #извлекаем числов вопросов квиза
            self.__quiz_count = int(self.__quiz_meta_arr[2].split(' ')[1])
            #извлекаем время на вопрос
            self.__quiz_time_per_question += self.__quiz_meta_arr[3].split(' ')[1] + ' '
            self.__quiz_time_per_question += self.__quiz_meta_arr[3].split(' ')[2]
        else:
            #извлекаем имя квиза
            self.__quiz_name = self.__quiz_meta_arr[0][self.__quiz_meta_arr[0].index('«')+1:-1]
            #извлекаем описание квиза
            self.__quiz_desc = ' '
            #извлекаем числов вопросов квиза
            self.__quiz_count = int(self.__quiz_meta_arr[2].split(' ')[1])
            #извлекаем время на вопрос
            self.__quiz_time_per_question += self.__quiz_meta_arr[2].split(' ')[1] + ' '
            self.__quiz_time_per_question += self.__quiz_meta_arr[2].split(' ')[2]

        #создаём папку назначения
        temp_name = ''
        temp_quiz = re.sub(r'[^A-zА-я0-9]','',self.__quiz_name)
        if len(temp_quiz) > 14:
            temp_name+=temp_quiz[:7]
            temp_name+='...'
            temp_name+=temp_quiz[-7:]
        else:
            temp_name+=temp_quiz

        temp_num_name = 1
        while os.path.exists(os.getcwd() + '/' + temp_name):
            if '_' in temp_name:
                temp_name = temp_name[:-1-len(str(temp_num_name-1))] + '_' + str(temp_num_name)
            else:
                temp_name += '_' + str(temp_num_name)
            temp_num_name += 1


        self.dir_name = temp_name
        os.mkdir(os.getcwd() + '/' + self.dir_name)


    def __meta_to_json(self):
        self.__quiz_json.append({'name': self.__quiz_name, 'description': self.__quiz_desc, \
                                 'quiz_count': self.__quiz_count, 'time_per_quiz': self.__quiz_time_per_question})

    # def __empty_quiz_to_json(self):
    #     self.__quiz_json.update({'quiz': {}})

    def __pull_to_json(self,name, answers, correct_answer):
        new_pull = {}
        new_pull_name = name
        new_pull.update({new_pull_name: {}})

        for answer in answers:
            new_pull[new_pull_name].update({'a_'+str(answers.index(answer)): answer})
        self.__answers_arr = []

        new_pull[new_pull_name].update({'correct_a': correct_answer})

        new_pull[new_pull_name].update({'solution': self.__solution})



        #добавляем мультимедию
        if self.media_flag:
            if str(type(self.media.media)) == "<class 'telethon.tl.types.MessageMediaPhoto'>" or str(type(self.media.media)) == "<class 'telethon.tl.types.MessageMediaDocument'>":
                media_int = random.randint(1, 999999)
                while media_int in self.media_name:
                    media_int = random.randint(1, 999999)
                self.media_name.append(media_int)
                path = os.getcwd() + '/' + self.dir_name + '/' + str(media_int)
                self.client.download_media(self.media, path,)
                new_pull[new_pull_name].update({'media_name': str(media_int)+".jpg"})
            new_pull[new_pull_name].update({'media_desc': self.media.message})
            self.media_flag = False

        self.__quiz_json.append(new_pull)

    def __data_to_json(self,data):
        path = os.getcwd() + '/' + self.dir_name + '/' + 'quiz.json'
        with open(path, "w",encoding="utf-8") as write_file:
            json.dump(data, write_file, ensure_ascii=False)

    def __send_msg_to_bot(self,msg):

        self.client.send_message(self.__quiz_bot_username, msg)



if __name__ == '__main__':
    #Заходим в https://my.telegram.org/apps
    #Заполняем поля  App title и Short name нажимаем «Create application» и запоминаем две переменные: api_id и api_hash.
    api_id = 
    api_hash = 
    #Вводим номер на который зарегестрирован телеграм аккаунт
    phone = 
    #Вводим имя сессии
    session_name = 
    #Ссылка которая даётся при создании квиза к примеру http://t.me/QuizBot?start=WFS8WSbw
    quiz_link = 'http://t.me/QuizBot?start=WFS8WSbw'


    tg_clinet = telegram_handler(api_id, api_hash, phone, session_name)
    tg_clinet.quiz_to_json(quiz_link)
