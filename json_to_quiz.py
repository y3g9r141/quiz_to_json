import json
from time import sleep

from telethon import TelegramClient, sync
from telethon.tl import types, functions


class telegram_handler:

    def __init__(self, api_id, api_hash, phone, session_name, path):
        """
        Конструктор класса устанавливает соединение с telegram аккаунтом согласно переданным параметрам
        """
        self.api_id = api_id
        self.api_hash = api_hash
        self.phone = phone
        self.session_name = session_name

        self.quiz_bot_username = 'QuizBot'

        self.path = path
        self.quiz_name = ''
        self.quiz_desc = ''
        self.quiz_count = 0
        self.quiz_time_per_question = ''
        self.quiz_questions = list()

        self.client = TelegramClient(self.session_name, self.api_id, self.api_hash)
        self.client.start()

    def quiz_out_json(self):
        with open(f"{self.path}/quiz.json", "r") as file:
            data = json.load(file)
        self.quiz_name = data[0]['name']
        self.quiz_desc = data[0]['description']
        self.quiz_count = data[0]['quiz_count']
        self.quiz_time_per_question = data[0]['time_per_quiz']
        self.quiz_questions = data[1:]

    def send_quizzes(self):
        for q in self.quiz_questions:
            question = list(q.keys())[0]
            correct_answer = q[question].pop('correct_a')
            solution = q[question].pop('solution', None)
            media_name = q[question].pop('media_name', None)
            if media_name:
                media_name = f'{self.path}/{media_name}'
            media_desc = q[question].pop('media_desc', '')
            if media_desc or media_name:
                self.client.send_message(self.quiz_bot_username, file=media_name, message=media_desc)
                messages = self.client.get_messages(self.quiz_bot_username)
                if messages[0].message == 'Отображать в этом сообщении расширенный предпросмотр для ссылки?':
                    messages[0].click(0)
                    sleep(1)
            answers = [types.PollAnswer(text=value, option=bytes([int(key[-1])])) for key, value in q[question].items()]
            poll = types.Poll(quiz=True, public_voters=True, answers=answers, id=10, question=question)
            media_poll = types.InputMediaPoll(poll=poll, correct_answers=[bytes([correct_answer])], solution=solution,
                                              solution_entities=[types.MessageEntityUnknown(1,1)])
            self.client.send_message(self.quiz_bot_username, file=media_poll)
            sleep(1)

    def create_test(self):
        self.quiz_out_json()
        self.send_msg_to_bot('/newquiz')
        self.send_msg_to_bot(self.quiz_name)
        if not self.quiz_desc or self.quiz_desc.isspace():
            self.send_msg_to_bot('/skip')
        else:
            self.send_msg_to_bot(self.quiz_desc)
        self.send_quizzes()
        self.send_msg_to_bot('/done')
        quiz_time = self.quiz_time_per_question.split()
        self.send_msg_to_bot(f'{quiz_time[0]} {quiz_time[1][:3]}')
        self.send_msg_to_bot('По порядку')

    def send_msg_to_bot(self, msg):
        self.client.send_message(self.quiz_bot_username, msg)


if __name__ == '__main__':
    #Заходим в https://my.telegram.org/apps
    #Заполняем поля  App title и Short name
    #нажимаем «Create application» и запоминаем две переменные: api_id и api_hash.
    api_id =
    api_hash =
    #Вводим номер на который зарегестрирован телеграм аккаунт
    phone =
    #Вводим имя сессии
    session_name =
    # путь к папке с данными для теста
    path =

    tg_clinet = telegram_handler(api_id, api_hash, phone, session_name, path)
    tg_clinet.create_test()